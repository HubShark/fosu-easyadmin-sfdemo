<?php 
// src/AppBundle/Form/CommentType.php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextAreaType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class CommentType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('commentAuthor', TextType::class)
            ->add('commentAuthorEmail', TextType::class)
            ->add('commentContent', CKEditorType::class)
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Comment'
        ));
    }

    public function getBlockPrefix()
    {
        return 'comment';
    }
}
